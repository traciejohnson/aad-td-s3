# Start from base image
FROM python:3.9.4

# Assign working directory
WORKDIR /aad-td-s3

# Install system-level packages
RUN pip3 install --upgrade pip \
    && pip3 install pyarrow==5.0.0 \
    && pip3 install snowflake-sqlalchemy 

# Install language-specific packages
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# Copy over your code
COPY . .

CMD ["python", "Run_All.py/"]

# Set entry
RUN chgrp -Rf root $HOME && chmod -Rf g+wx $HOME \
&& chmod +x ./entrypoint.sh 

ENTRYPOINT ["./entrypoint.sh"]