# Import required packages for AWS S3 / Set up bucket path
import boto3
client = boto3.client('s3')
s3_path = '/national-account-sales-and-service/Snowflake-Stage/'

# Import required packages for teradata / snowflake
import teradatasql as td
import snowflake.connector
import pandas as pd
from datetime import datetime
from datetime import date
pd.options.mode.chained_assignment = None  # default='warn'
import numpy as np
import os
import csv

TDUser = os.environ.get('TERADATA_USER')
TDPass = os.environ.get('TERADATA_PASS')
SFUser = os.environ.get('SNOWFLAKE_USER')
SFPass = os.environ.get('SNOWFLAKE_PASS')

from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

# Snowflake Connection Configuration
account = 'wwgrainger.us-east-1'
warehouse = 'NASS_WH_M'
database = 'NASS'
schema = 'TERADATA_RAW'
role = 'NASS_SVC'

engine = create_engine(URL(
    account = account,
    #authenticator = 'oauth',
    user = SFUser,
    password = SFPass,
    database = database,
    schema = schema,
    warehouse = warehouse,
    role=role,
    #token = access_token
))

#for inserting and reading data
sf_connection = engine.connect()
#for creating and dropping tables
sf_cursor_conn = engine.raw_connection()
sf_cursor = sf_cursor_conn.cursor()

tdCon = td.connect(None,host="prtrdatacop1.sap.grainger.com",user=TDUser,password=TDPass)

sysdate = str(date.today())

os.chdir('source-sql')
for a in os.listdir():
    try:
        print('Begin Loading to S3 - '+a)
        fd = open(a,'r')
        sqlFile = fd.read()
        fd.close()
        target_table_nm=a[:-4]
        file_name = str(target_table_nm)+'-'+sysdate+'.csv'
        s3_key_path = s3_path+'/'+file_name
        client.delete_object(Bucket='aad-grainger-prod', Key=s3_key_path)
        chunksize = 500000
        i = 0
        j= 0
        #Create Batch TS Field for Staging table-1
        current_ts = datetime.now()
        now = current_ts.strftime("%Y-%m-%d %H:%M:%S")
        for df in pd.read_sql(sqlFile, tdCon, chunksize=chunksize):
            df.index += j
            #Create Batch TS Field for Staging table-2            
            df["ZBatch_TS"] = now
            #Create S3 File for current date loaded
            df.to_csv(file_name,mode='a',index=False, sep ='~', quoting=csv.QUOTE_NONE, escapechar=' ',header=False)
            #using pandas dataframe, create staging table if it does not currently exist
            df.head(0).to_sql(target_table_nm,con=sf_connection, if_exists='append', index=False)
            j = df.index[-1]+1
            print('| index: {}'.format(j))
        client.upload_file(file_name,'aad-grainger-prod', s3_key_path)
        os.remove(file_name)
    except:
        print(a+' did not load due to errors in the file')
        pass
print('All AAD-TD-S3 Jobs Complete')
